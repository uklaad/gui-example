#include "include/app_design.h"
#include "icecream.hpp"

#include <cmath>
#include <vector>
#include <string>

class Parameters
{
public:
    void parameterWindow()
    {
        ImGui::Begin("Set parameters");
        // ImGui::InputScalar("Variable A", ImGuiDataType_Float, &VarA);
        ImGui::SliderFloat("Variable A", &VarA, -100, 100);
        ImGui::Text("Variable A: %f", VarA); // https://en.wikibooks.org/wiki/C%2B%2B_Programming/Code/Standard_C_Library/Functions/printf
        // ImGui::InputScalar("Variable B", ImGuiDataType_Float, &VarB);
        ImGui::SliderFloat("Variable B", &VarB, -100, 100);
        ImGui::Text("Variable B: %f", VarB);
        ImGui::Dummy(ImVec2(0.0f, 10.0f));

        ImGui::Text("A+B = %f", VarA + VarB);

        ImGui::Checkbox("Show graph", &ShowGraph);
        if (ShowGraph)
        {
            for (int i = 0; i < 1000; ++i)
            {
                X[i] = i * VarA;
                Y[i] = VarB * X[i];
            }
            if (ImPlot::BeginPlot("##XY_Plot"))
            {
                // ImPlot::PlotBars("##",bar_data , 4);
                ImPlot::SetupAxes("X axis", "Y axis", ImPlotAxisFlags_AutoFit, ImPlotAxisFlags_AutoFit);
                ImPlot::PlotLine("##XY_Plot", X, Y, 1000);

                ImPlot::EndPlot();
            }
        }

        ImGui::End();
    }

    void setVarA(double varA) { VarA = varA; }
    void setVarB(double varB) { VarB = varB; }
    float getVarA() { return VarA; }
    float getVarB() { return VarB; }

private:
    float VarA;
    float VarB;
    float X[1000], Y[1000];
    bool ShowGraph = true;
};

struct ExampleAppLog
{
    ImGuiTextBuffer Buf;
    ImGuiTextFilter Filter;
    ImVector<int> LineOffsets; // Index to lines offset. We maintain this with AddLog() calls.
    bool AutoScroll;           // Keep scrolling if already at the bottom.
    bool add = false;

    ExampleAppLog()
    {
        AutoScroll = true;
        Clear();
    }

    void Clear()
    {
        Buf.clear();
        LineOffsets.clear();
        LineOffsets.push_back(0);
    }

    void AddLog(const char *fmt, ...) IM_FMTARGS(2)
    {
        int old_size = Buf.size();
        va_list args;
        va_start(args, fmt);
        Buf.appendfv(fmt, args);
        va_end(args);
        for (int new_size = Buf.size(); old_size < new_size; old_size++)
            if (Buf[old_size] == '\n')
                LineOffsets.push_back(old_size + 1);
    }

    void Draw(const char *title, bool *p_open = NULL)
    {
        if (!ImGui::Begin(title, p_open))
        {
            ImGui::End();
            return;
        }

        // Main menu
        ImGui::Checkbox("Auto-scroll", &AutoScroll);
        ImGui::SameLine();
        add = ImGui::Button("Add");
        ImGui::SameLine();
        bool clear = ImGui::Button("Clear");
        ImGui::SameLine();
        bool copy = ImGui::Button("Copy");
        ImGui::SameLine();
        Filter.Draw("Filter", -100.0f);

        ImGui::Separator();

        if (ImGui::BeginChild("scrolling", ImVec2(0, 0), false, ImGuiWindowFlags_HorizontalScrollbar))
        {
            if (clear)
                Clear();
            if (copy)
                ImGui::LogToClipboard();

            ImGui::PushStyleVar(ImGuiStyleVar_ItemSpacing, ImVec2(0, 0));
            const char *buf = Buf.begin();
            const char *buf_end = Buf.end();
            if (Filter.IsActive())
            {
                // In this example we don't use the clipper when Filter is enabled.
                // This is because we don't have random access to the result of our filter.
                // A real application processing logs with ten of thousands of entries may want to store the result of
                // search/filter.. especially if the filtering function is not trivial (e.g. reg-exp).
                for (int line_no = 0; line_no < LineOffsets.Size; line_no++)
                {
                    const char *line_start = buf + LineOffsets[line_no];
                    const char *line_end = (line_no + 1 < LineOffsets.Size) ? (buf + LineOffsets[line_no + 1] - 1) : buf_end;
                    if (Filter.PassFilter(line_start, line_end))
                        ImGui::TextUnformatted(line_start, line_end);
                }
            }
            else
            {
                // The simplest and easy way to display the entire buffer:
                //   ImGui::TextUnformatted(buf_begin, buf_end);
                // And it'll just work. TextUnformatted() has specialization for large blob of text and will fast-forward
                // to skip non-visible lines. Here we instead demonstrate using the clipper to only process lines that are
                // within the visible area.
                // If you have tens of thousands of items and their processing cost is non-negligible, coarse clipping them
                // on your side is recommended. Using ImGuiListClipper requires
                // - A) random access into your data
                // - B) items all being the  same height,
                // both of which we can handle since we have an array pointing to the beginning of each line of text.
                // When using the filter (in the block of code above) we don't have random access into the data to display
                // anymore, which is why we don't use the clipper. Storing or skimming through the search result would make
                // it possible (and would be recommended if you want to search through tens of thousands of entries).
                ImGuiListClipper clipper;
                clipper.Begin(LineOffsets.Size);
                while (clipper.Step())
                {
                    for (int line_no = clipper.DisplayStart; line_no < clipper.DisplayEnd; line_no++)
                    {
                        const char *line_start = buf + LineOffsets[line_no];
                        const char *line_end = (line_no + 1 < LineOffsets.Size) ? (buf + LineOffsets[line_no + 1] - 1) : buf_end;
                        ImGui::TextUnformatted(line_start, line_end);
                    }
                }
                clipper.End();
            }
            ImGui::PopStyleVar();

            // Keep up at the bottom of the scroll region if we were already at the bottom at the beginning of the frame.
            // Using a scrollbar or mouse-wheel will take away from the bottom edge.
            if (AutoScroll && ImGui::GetScrollY() >= ImGui::GetScrollMaxY())
                ImGui::SetScrollHereY(1.0f);
        }
        ImGui::EndChild();
        ImGui::End();
    }
};

class ReportWindow
{
public:
    void addReport(Parameters &parameters)
    {
        if (report.add)
        {
            report.AddLog("Sum is %f\n", parameters.getVarA() + parameters.getVarB());
        }
        report.Draw("Report");
    }

private:
    ExampleAppLog report;
};

class MyApp : public App<MyApp>
{
public:
    MyApp() = default;
    ~MyApp() = default;

    //### Your app begins here ###
    virtual void StartUp() final
    {
    }
    virtual void Update() final
    {
        ImGui::ShowDemoWindow();

        parameters.parameterWindow();
        reportWindow.addReport(parameters);
    }

private:
    Parameters parameters;
    ReportWindow reportWindow;

    //### Your app ends here ###
};

int main(int, char **)
{
    MyApp app;
    app.Run();

    return 0;
}
