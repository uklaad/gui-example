**Purpose:** GUI example program. This code shows you how to make GUI C++ program. The manual is written for Windows x64 builds.

**Prerequisites**

1. Optional [Git](https://git-scm.com/downloads) (let it be all default, just hit next)
2. [MSys2](https://www.msys2.org/) (MinGw)
   * after installation it automatically runs MSYS2 UCRT64 window, so paste this command:
     `pacman -S mingw-w64-ucrt-x86_64-gcc mingw-w64-x86_64-cmake mingw-w64-x86_64-glew mingw-w64-x86_64-glfw --needed base-devel mingw-w64-x86_64-toolchain`
     and let the list be default (hit enter), confirm with Y (Yes)
   * add folder to path: `Control Panel -> System and Security -> System -> Advanced system settings -> Environment Variables`
     see [here](https://code.visualstudio.com/docs/cpp/config-mingw) paragraph 6.
3. [VS Code](https://code.visualstudio.com/download)
   * install extensions: `C/C++ Extension Pack, Better Syntax, CMake Language Support, Office Viewer`
   * To use c++20 in code review: `Ctrl+, -> cppstandard -> change the default settings`

**Download, compile and run**

1. Download this [example](https://gitlab.com/uklaad/gui-example)
   * if you installed [Git](https://git-scm.com/downloads), clone git repo to VS Code; if not, just download zip and extract
   * select working dir for the project, open project (trust authors), let it autoconfigure (it should download all necessary stuff), set GCC from menu as a compiler
2. After configuration hit F7 (Build in taskbar) and it should compile with exit code 0
3. Run the program by Shift+F5 (triangle in taskbar) or just open the executable in the program folder

**Useful links:**

* [GLEW](https://stackoverflow.com/questions/27472813/linking-glew-with-cmake)
* Static linking [[1]](https://stackoverflow.com/questions/6404636/libstdc-6-dll-not-found), [[2]](https://stackoverflow.com/questions/13768515/how-to-do-static-linking-of-libwinpthread-1-dll-in-mingw)
* [CPM](https://medium.com/swlh/cpm-an-awesome-dependency-manager-for-c-with-cmake-3c53f4376766)

**Some good additional info:**

* [VSC begin](https://www.youtube.com/watch?v=FcYs8wtzjVE) ; [C++ full course](https://www.youtube.com/watch?v=6y0bp-mnYU0) ; [Obj C++](https://www.youtube.com/watch?v=wN0x9eZLix4) ; [Md Flowcharts](https://mermaid-js.github.io/mermaid/#/flowchart)

---

**Program window**

![1668720009259](image/README/1668720009259.png)
